/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.*;
import java.io.Serializable;
import java.util.Date;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;
/**
 *
 * @author st
 */
public class ConsultationBean implements Consultation, Serializable {
    private String student;
    private Term term;

    public ConsultationBean() {
        this.term = new TermBean();
    }

    public ConsultationBean(String student, Term term) {
        this.student = student;
        this.term = term;
    }

    public Term getTerm() {
        return this.term;
    }

    @Override
    public String getStudent() {
        return this.student;
    }

    @Override
    public Date getBeginDate() {
        return this.term.getBegin();
    }

    @Override
    public Date getEndDate() {
        return this.term.getEnd();
    }

    @Override
    public void setTerm(Term term) throws PropertyVetoException {
        this.term = term;
    }

    @Override
    public void setStudent(String student) {
        this.student = student;
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException {
        if (minutes > 0) {
            this.term.setDuration(this.term.getDuration() + minutes);
        }
    }
}
