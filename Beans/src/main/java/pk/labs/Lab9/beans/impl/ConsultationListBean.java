/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;
import java.beans.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import pk.labs.Lab9.beans.*;
/**
 *
 * @author st
 */
public class ConsultationListBean implements ConsultationList, Serializable {
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    private List<Consultation> consultationList;

    public ConsultationListBean() {
        this.consultationList = new ArrayList<Consultation>();
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.addPropertyChangeListener(listener);
    }

    @Override
    public int getSize() {
        return this.consultationList.size();
    }

    @Override
    public Consultation[] getConsultation() {
        return this.consultationList.toArray(new Consultation[this.consultationList.size()]);
    }

    @Override
    public Consultation getConsultation(int index) {
        return this.consultationList.get(index);
    }

    @Override
    public void addConsultation(Consultation newConsultation) throws PropertyVetoException {

        this.consultationList.add(newConsultation);
        this.propertyChangeSupport.firePropertyChange("consultation", null, this.consultationList);
    }
}
