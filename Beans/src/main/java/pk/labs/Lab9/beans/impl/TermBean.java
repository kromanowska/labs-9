/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import pk.labs.Lab9.beans.Term;
/**
 *
 * @author st
 */
public class TermBean implements Term, Serializable {
    
    private Date begin;
    private Date end;
    private int duration;

    public TermBean() {
        this.begin = new Date();
        this.end = new Date();
        this.duration = 0;
    }

    public TermBean(Date begin, int duration) {
        this.begin = begin;
        this.duration = duration;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(begin);
        calendar.add(Calendar.MINUTE, duration);
        this.end = calendar.getTime();
    }

    @Override
    public Date getBegin() {
        return this.begin;
    }

    @Override
    public void setBegin(Date begin) {
        this.begin = begin;
    }

    @Override
    public int getDuration() {
        return this.duration;
    }

    @Override
    public void setDuration(int duration) {
        if (duration > 0) {
            this.duration = duration;
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(begin);
            calendar.add(Calendar.MINUTE, duration);
            this.end = calendar.getTime();
        }
    }

    @Override
    public Date getEnd() {
        return this.end;
    }
}
