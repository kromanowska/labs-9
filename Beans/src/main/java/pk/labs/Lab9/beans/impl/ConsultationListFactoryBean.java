/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;
/**
 *
 * @author st
 */
public class ConsultationListFactoryBean implements ConsultationListFactory {
     public ConsultationListFactoryBean() {

    }

    @Override
    public ConsultationList create() {
        return new ConsultationListBean();
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        ConsultationList list = create();
        if (deserialize) {
            try {
                XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream("Consultations.xml")));
                Consultation[] consultations = (Consultation[])decoder.readObject();
                for (Consultation consultation : consultations) {
                    list.addConsultation(consultation);
                }
                decoder.close();
            } catch (Exception ex) {

            }
        } 
        return list;
    }

    @Override
    public void save(ConsultationList consultationList) {
        try {
            XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("Consultations.xml")));
            encoder.writeObject(consultationList.getConsultation());
            encoder.close();
        } catch (Exception ex) {
            
        }
    }
}
